SHELL = /bin/sh
EMACS = emacs

FILES = $(wildcard *.el)

LIBS =

ELCFILES = $(FILES:.el=.elc)

.PHONY: all compile

# Byte-compile rc files.
all: compile
compile: $(ELCFILES)

$(ELCFILES): %.elc: %.el
	@$(EMACS) --batch -Q -L . $(LIBS) -f batch-byte-compile $<

# Delete byte-compiled files etc.
clean:
	rm -f *~
	rm -f \#*\#
	rm -f $(filter-out dropbox-sync.elc,$(wildcard *.elc))
